﻿using System;
using System.IO;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
    public string nombreArchivo;
    public Material defMatMusculo;
    public Material defMatHueso;

    // Start is called before the first frame update
    void Start()
    {
        crearTodo();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void crearTodo()
    {
        string path = "./Assets/" + nombreArchivo + ".json";

        StreamReader reader = new StreamReader(path);
        ObjetoSerializable myObject = JsonUtility.FromJson<ObjetoSerializable>(reader.ReadToEnd());
        reader.Close();

        crearObjeto(myObject, null);
        
    }

    private void crearObjeto(ObjetoSerializable objOrigen, GameObject padre)
    {
        GameObject nuevo = new GameObject { name = objOrigen.nombre };

        if(padre != null) nuevo.transform.parent = padre.transform;

        nuevo.transform.localRotation = new Quaternion(objOrigen.rotX, objOrigen.rotY, objOrigen.rotZ, objOrigen.rotW);
        nuevo.transform.localPosition = objOrigen.posicion;
        nuevo.transform.localScale = objOrigen.escala;

        if (objOrigen.tieneMeshFilter)
        {
            Mesh malla = new Mesh();
            nuevo.AddComponent<MeshFilter>().mesh = malla;

            malla.vertices = objOrigen.vertices;
            malla.uv = objOrigen.uv;
            malla.triangles = objOrigen.triangles;

            nuevo.AddComponent<MeshRenderer>();

            if(padre.name.Contains("Tejido")) nuevo.GetComponent<MeshRenderer>().material = defMatMusculo;
            else nuevo.GetComponent<MeshRenderer>().material = defMatHueso;
        }

        ObjetoSerializable[] hijos = objOrigen.objetosHijos;

        if (hijos != null && hijos.Length != 0)
        {
            for(int i = 0; i < hijos.Length; i++)
            {
                crearObjeto(hijos[i], nuevo);
            }

        }

    }


    [Serializable]
    public class ObjetoSerializable
    {
        public string nombre;
        public bool tieneMeshFilter;
        public ObjetoSerializable[] objetosHijos;
        public Vector3[] vertices;
        public Vector2[] uv;
        public int[] triangles;
        public float rotW;
        public float rotX;
        public float rotY;
        public float rotZ;
        public Vector3 posicion;
        public Vector3 escala;

    }

}

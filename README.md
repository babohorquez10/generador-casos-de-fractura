# Generador Casos de Fractura

Esta aplicación toma como base los archivos JSON generados en la aplicación [Extensión OrtHáptica](https://gitlab.com/babohorquez10/extension-orthaptica) los cuales describen el caso de fractura diseñado 
para así generar y exportar el caso de fractura para su uso en OrtHáptica.

[Demo](https://youtu.be/rSO4RZ1JhmE)

## Prerrequisitos:

Descargar e instalar [Unity](https://unity3d.com/get-unity/download) (la versión en la que se desarrolló el proyecto es la versión 2018.3.0f2).

## Instalación y ejecución del proyecto:

- Descargar el proyecto.
- Abrir el proyecto en Unity.
- Copiar el archivo JSON <nombre_del_caso>_Exportado.json (generado previamente por la aplicación [Extensión OrtHáptica](https://gitlab.com/babohorquez10/extension-orthaptica)) y pegarlo en el directorio Assets del proyecto.
- Abrir la escena GeneradorCasos (ubicada en Assets/scenes/GeneradorCasos) en Unity.
- En la ventana Hierarchy de la escena, la cual muestra la jerarquía de los diferentes elementos de la escena (usualmente ubicada a la izquierda),
  seleccionar el elemento "Manager" al dar click sobre este.
- En la ventana Inspector (normalmente desplegada en la parte derecha de la pantalla) cambiar la propiedad "Nombre Archivo" del componente 
  "Object Generator (Script)" y poner el nombre del archivo JSON (sin la extensión .json) del caso de fractura. Por ejemplo, si el archivo JSON 
  se llama caso01_Exportado.json se debe escribir caso01_Exportado en la propiedad "Nombre Archivo" del componente "Object Generator (Script)".
- Ejecutar la escena GeneradorCasos.
- Al ejecutar la escena, se creará automáticamente el objeto del caso de fractura en la jerarquía de la escena.
  Para ver el objeto se puede ver el panel "Hierarchy" de la escena, en donde el objeto creado tendrá el nombre del caso de fractura.
- En el panel "Hierarchy" de la escena dar click derecho sobre el objeto generado (nombrado igual que el caso de fractura) y seleccionar la opción
  Export to FBX...
- Seleccionar la carpeta de destino y seleccionar la opción Export.
- De esta forma, se exportará el objeto del caso de fractura en formato FBX y quedará guardado en la ubicación seleccionada.

## Importar el caso de fractura en OrtHáptica:

La importación del caso de fractura en OrtHáptica se realiza a partir del objeto en formato FBX generado en el paso anterior y los archivos JSON generados 
por la aplicación [Extensión OrtHáptica](https://gitlab.com/babohorquez10/extension-orthaptica), por lo que estos tres archivos deben ser proporcionados al integrar el caso de fractura en OrtHáptica.